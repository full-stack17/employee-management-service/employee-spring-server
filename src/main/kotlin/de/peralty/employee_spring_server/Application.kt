package de.peralty.employee_spring_server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EmployeeSpringServerApplication

fun main(args: Array<String>) {
    runApplication<EmployeeSpringServerApplication>(*args)
}
